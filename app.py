from geopy.geocoders import Nominatim
from geopy.distance import geodesic
import requests
from bs4 import BeautifulSoup
from collections import Counter
from string import punctuation
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import webbrowser
from geopy.distance import great_circle
plotly.tools.set_credentials_file(username='makers-league', api_key='AIzaSyDmnVTQlokAqF_X6qn705nmubujcbTk1Fk')


class City:
#City objects are used as vertices in the MapGraph class.
    def __init__(self, n):
        self.name = n


class MapGraph:
#MapGraph is a class for constructing weighted undirected graphs using cities as vertices.
    def __init__(self):
        self.cities = {}
        self.edges = []
        self.adj_list = {}
        self.cities_indices = {}

    def add_road(self, city_x, city_y, distance):
    #To construct a new graph add all roads between cities
        city_x = city_x.lower().strip()
        city_y = city_y.lower().strip()

        if city_x == city_y:
            return
        if city_x not in self.cities:
            new_city_x = City(city_x)
            self.cities[new_city_x.name] = new_city_x
            self.adj_list[new_city_x.name] = []
            for row in self.edges:
                row.append(0)
            self.edges.append([0] * (len(self.edges) + 1))
            self.cities_indices[new_city_x.name] = len(self.cities_indices)

        if city_y not in self.cities:
            new_city_y = City(city_y)
            self.cities[new_city_y.name] = new_city_y
            self.adj_list[new_city_y.name] = []
            for row in self.edges:
                row.append(0)
            self.edges.append([0] * (len(self.edges) + 1))
            self.cities_indices[new_city_y.name] = len(self.cities_indices)

        if city_y not in self.adj_list[city_x]:
            self.adj_list[city_x].append(city_y)
        if city_x not in self.adj_list[city_y]:
            self.adj_list[city_y].append(city_x)

        self.edges[self.cities_indices[city_x]][self.cities_indices[city_y]] = distance
        self.edges[self.cities_indices[city_y]][self.cities_indices[city_x]] = distance

    def find_all_paths(self, city_x, city_y, path=[]):
    #This method is used as a backend method to support other methods in the class

        city_x = city_x.lower().strip()
        city_y = city_y.lower().strip()

        path = path + [city_x]

        if city_x not in self.cities:
            return []

        if city_x == city_y:
            return [path]

        paths = []
        newpaths = []

        for city in self.adj_list[city_x]:
            if city not in path:
                newpaths = self.find_all_paths(city, city_y, path)
            for newpath in newpaths:
                if (len(newpath) < 4 or len(newpath) > 5):
                    pass
                else:
                    paths.append(newpath)
        return paths

    def all_possible_paths(self, city_x, city_y):
    #To get all possible paths between 2 cities in the driver code->restricts the possible paths to be transitting through 2-3 cities only


        all_possible_paths = self.find_all_paths(city_x, city_y)
        all_possible_paths = [list(t) for t in set(tuple(element) for element in all_possible_paths)]
        return all_possible_paths

    def shortest_path(self, city_x, city_y):
    #This method returns a list of (shortest path index, shortest path, distance of shortest path)

        all_paths = self.all_possible_paths(city_x, city_y)
        shortets_value = 10 ** 9
        shortest_path = []
        count = 0
        x = 0
        for each_path in all_paths:
            path_distance = 0
            count += 1
            for i in range(0, len(each_path) - 1):
                path_distance += self.edges[self.cities_indices[each_path[i]]][self.cities_indices[each_path[i + 1]]]
            if path_distance < shortets_value:
                shortets_value = path_distance
                shortest_path = each_path
                x = count
        if shortets_value == 10 ** 9:
            shortets_value = 0
        return [x, shortest_path, shortets_value]

    def print_graph(self):
        """This method prints the graph"""


#DRIVER CODE -> randomized grapgh between 10 different states & finds shortest path between kl and madrid

g = MapGraph()
list1 = ["KLCC", "KL Central", "Pasar Seni", "Bukit Bintang", "Ampang Park"]
geolocator = Nominatim(user_agent="specify_your_app_name_here")
counter1 = 0
counter2 = 0
counter3 = 0
location = []
longlat = []

for x in range(len(list1)):
    location.append(geolocator.geocode(list1[x]))
    longlat.append((location[x].latitude, location[x].longitude))

for city1 in list1:
    for city2 in list1:
        y = int(geodesic(longlat[counter2], longlat[counter3]).miles)
        g.add_road(city1, city2, y)
        counter3 += 1
    counter3 = 0
    counter2 += 1
g.print_graph()

print()
paths = g.all_possible_paths("Madrid", "Moscow")
for path in paths:
    print(path)
shortest = g.shortest_path("Madrid", "Moscow")
print("\n")
print(shortest)



print(shortest[1][0])

#imports

from gmplot import gmplot

gmap = gmplot.GoogleMapPlotter(location[0].latitude, location[0].longitude, 2)

#place a marker for each city
top_attraction_lats, top_attraction_lons = zip(*[
    (location[0].latitude, location[0].longitude),
    (location[1].latitude, location[1].longitude),
    (location[2].latitude, location[2].longitude),
    (location[3].latitude, location[3].longitude),
    (location[4].latitude, location[4].longitude),
    (location[5].latitude, location[5].longitude),
    (location[6].latitude, location[6].longitude),
    (location[7].latitude, location[7].longitude),
    (location[8].latitude, location[8].longitude),
    (location[9].latitude, location[9].longitude)
    ])
gmap.scatter(top_attraction_lats, top_attraction_lons, '#00ff00', size=99999, marker=False)



def location23(name):
    wer = 0
    for xyz in list1:
        if(list1[wer].lower()==name.lower()):
            return location[wer]
        wer = wer +1

if(len(shortest)==3):
    templocation1 = location23(shortest[1][0])
    templocation2 = location23(shortest[1][1])
    templocation3 = location23(shortest[1][2])
    templocation4 = location23(shortest[1][3])
    a, b = zip(*[
    (templocation1.latitude, templocation1.longitude),
    (templocation2.latitude, templocation2.longitude),
    (templocation3.latitude, templocation3.longitude),
    (templocation4.latitude, templocation4.longitude)
    ])
    gmap.plot(a, b, 'c', edge_width=9)



gmap.draw("MAP.html")
webbrowser.open("MAP.html")

positive_vocab = open('positive.txt', 'r').read().split(', ')  # Replace with text file of positive words
negative_vocab = open('negative.txt', 'r').read().split(', ')
stops_vocab = open('stops.txt', 'r').read().split('\n')

r = requests.get("https://www.euronews.com/tag/madrid") # put any website here

soup = BeautifulSoup(r.content,features="html.parser")

text = (''.join(s.findAll(text=True))for s in soup.findAll('p'))

c = Counter((x.rstrip(punctuation).lower() for y in text for x in y.split()))
print (c.most_common()) # prints most common words staring at most common.
mostCommon = c.most_common()


trace2 = go.Scatter(
    x = c.keys(),
    y = c.values(),
    mode = 'markers'
)
#print(random_x)
data2 = [trace2]


#trace2 = go.Bar(
#    x = c.keys(),
#    y = c.values()

#)
# print(random_x)
#data2 = [trace2]

# Plot and embed in ipython notebook!
py.iplot(data2, filename=' -2 Bar Graph', auto_open=True)

neg = 0
pos = 0
stops = 0
neutral = 0

i = 0


for word in mostCommon:
    word = mostCommon[i][0]
    for x in range(len(positive_vocab)):
        if( word == positive_vocab[x].lower()):
            pos = pos + mostCommon[i][1]
            # print(word)#testing

    for y in range(len(negative_vocab)):
        if(word == negative_vocab[y].lower()):
            neg= neg+ mostCommon[i][1]
            # print(word)  # testing
    for z in range(len(stops_vocab)):
        if(word == stops_vocab[z].lower()):
            stops= stops+ mostCommon[i][1]
    i=i+1


i = 0
total = 0
for words in mostCommon:
    total = total + mostCommon[i][1]
    i += 1


# print(len(mostCommon))  # for testing
print(total)
print("negative ",neg)  # for testing
print("positive ",pos)  # for testing
print("stops ",stops)  # for testing
print("neutral ",total-neg-pos-stops)  # for neutral
if str(float(pos) / len(mostCommon)) > str(float(neg) / len(mostCommon)):
    print("The artical is positive")
else:
    print("The artical is negative")

both = [("Negative","positive"),(neg,pos)]



trace = go.Bar(
    x = both[0],
    y = both[1]

)
# print(random_x)
data = [trace]

# Plot and embed in ipython notebook!
py.iplot(data, filename=' - Bar Graph', auto_open=True)
# trace2 = {
#   x= [neg, pos],
#
# }
# data2 = [trace2]
#
# plot_url = py.iplot(data2, filename='basic histogram2')



location1 = geolocator.geocode("KLCC")
location2 = geolocator.geocode("KL Central")
location3 = geolocator.geocode("Pasar Seni")
location4 = geolocator.geocode("Bukit Bintang")
location5 = geolocator.geocode("Ampang Park")


location1 = (location1.latitude, location1.longitude)
location2 = (location2.latitude, location2.longitude)
location3 = (location3.latitude, location3.longitude)
location4 = (location4.latitude, location4.longitude)
location5 = (location5.latitude, location5.longitude)
location6 = (location6.latitude, location6.longitude)
location7 = (location7.latitude, location7.longitude)
location8 = (location8.latitude, location8.longitude)
location9 = (location9.latitude, location9.longitude)
location10 = (location10.latitude, location10.longitude)

#dest of the vertices
dest1 = great_circle(location1, location2, location4).km
dest2 = great_circle(location1, location3, location4).km
dest3 = great_circle(location1, location5, location4).km
dest4 = great_circle(location1, location6, location4).km
dest5 = great_circle(location1, location7, location4).km
dest6 = great_circle(location1, location8, location4).km
dest7 = great_circle(location1, location9, location4).km
dest8 = great_circle(location1, location10, location4).km

total_distance = (dest1 + dest2 + dest3 + dest4 + dest5 + dest6 + dest7 + dest8)

prob_dest1 = dest1/ total_distance
print (prob_dest1)
